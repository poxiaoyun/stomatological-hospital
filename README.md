# 口腔医院义齿订单管理系统

#### 介绍
口腔医院通过系统发送义齿订单到加工中心进行加工，同时对义齿的制作过程进行监控，即义齿在加工过程中的工艺需在系统中实时记录，对医院可以通过系统来查看义齿的制作进度，同时通过订单来对医院和工厂的工作人员的月工作量进行汇总，主要功能：义齿订单模块，加工进度监控模块，工作量汇总模块，鼠标绘图功能、订单打印功能

#### 软件架构
采用 SpringBoot+MyBatis+MySQL+Shiro技术


#### 预览图片
- 登录

![输入图片说明](document/images/login.png)
- 医生

![输入图片说明](document/images/doctor1.png)
![输入图片说明](document/images/doctor2.png)
![输入图片说明](document/images/doctor3.png)
![输入图片说明](document/images/doctorList.png)
- 义齿订单添加页

![输入图片说明](document/images/orderAdd.png)
- 订单打印

![输入图片说明](document/images/orderPrint.png)
- 加工中心

![输入图片说明](document/images/t1.png)
![输入图片说明](document/images/t2.png)

- 人员管理页面

![输入图片说明](document/images/userManage.png)

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
