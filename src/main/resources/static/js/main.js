var clickX = [];
var clickY = [];
var clickDrag = [];
var paint;
var c = document.getElementById("canvas");
var context = c.getContext("2d");
var img = new Image();
img.src = "images/b.png";
img.onload = imgfn;//图片加载完在执行
function imgfn() {
    //createPattern() 方法在指定的方向内重复指定的元素。
    context.fillStyle = context.createPattern(img, "no-repeat");//fillStyle 属性设置或返回用于填充绘画的颜色、渐变或模式。
    context.fillRect(0, 0, c.width, c.height);//绘制已填充矩形fillRect(左上角x坐标, 左上角y坐标, 宽, 高)
}
//鼠标按下事件
$('#canvas').mousedown(function (e) {
    // var mouseX = e.pageX - this.offsetLeft;
    // var mouseY = e.pageY - this.offsetTop;
    var rect = c.getBoundingClientRect();
    //2
    var mouseX = e.clientX - rect.left * (c.width / rect.width);
    var mouseY = e.clientY - rect.top * (c.height / rect.height);
    paint = true;
    addClick(mouseX, mouseY,'');
    // 在这里储存绘图表面
    this.firstDot = context.getImageData(0, 0, c.width, c.height);
    saveData(this.firstDot);
    redraw();
});
//鼠标移动事件
$('#canvas').mousemove(function (e) {
    if (paint) {//是不是按下了鼠标
        var rect = c.getBoundingClientRect();
        //2
        var mouseX = e.clientX - rect.left * (c.width / rect.width);
        var mouseY = e.clientY - rect.top * (c.height / rect.height);
        addClick(mouseX, mouseY, true);
        redraw();
    }
});
//鼠标松开事件
$('#canvas').mouseup(function (e) {
    paint = false;
    clickX.splice(0, clickX.length);
    clickY.splice(0, clickY.length);
    clickDrag.splice(0, clickDrag.length);
});
//鼠标移开事件
$('#canvas').mouseleave(function (e) {
    paint = false;
    clickX.splice(0, clickX.length);
    clickY.splice(0, clickY.length);
    clickDrag.splice(0, clickDrag.length);
});

//addClick方法 记录鼠标坐标点
function addClick(x, y, dragging) {
    clickX.push(x);
    clickY.push(y);
    clickDrag.push(dragging);
}

//redraw方法
// 目前这个redraw方法是每次都清空画板，然后重新把所有的点都画过，虽然效率不高，但是这样看起来还是挺简单的。
function redraw() {
    // context.width = context.width; // 清除画布

    context.strokeStyle = "#0624ec";
    context.lineJoin = "round";
    context.lineWidth = 3;
    console.log("clickX：" + clickX);
    console.log("clickY：" + clickY);
    console.log("clickDrag：" + clickDrag);
    for (var i = 0; i < clickX.length; i++) {
        context.beginPath();
        if (clickDrag[i] && i) {//当是拖动而且i!=0时，从上一个点开始画线。
            context.moveTo(clickX[i - 1], clickY[i - 1]);
        } else {
            context.moveTo(clickX[i] - 1, clickY[i]);
        }
        context.lineTo(clickX[i], clickY[i]);
        context.closePath();
        context.stroke();
    }
}
// 清空
let reSetCanvas = document.getElementById("clear");
reSetCanvas.onclick = function () {
    context.clearRect(0, 0, c.width, c.height);
    clickX.splice(0, clickX.length);
    clickY.splice(0, clickY.length);
    clickDrag.splice(0, clickDrag.length);
    imgfn();
};

// 下载
let save = document.getElementById("save");
save.onclick = function () {
    let imgUrl = c.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "kqyy" + (new Date).getTime();
    saveA.target = "_blank";
    saveA.click();
};
// 撤销操作，只能撤销十步
let undo = document.getElementById("undo");
let historyDeta = [];

function saveData(data) {
    (historyDeta.length === 10) && (historyDeta.shift()); // 上限为储存10步，太多了怕挂掉
    historyDeta.push(data);
}

undo.onclick = function () {
    if (historyDeta.length < 1) return false;
    context.clearRect(0, 0, c.width, c.height);
    clickX.splice(0, clickX.length);
    clickY.splice(0, clickY.length);
    clickDrag.splice(0, clickDrag.length);
    imgfn();
    context.putImageData(historyDeta[historyDeta.length - 1], 0, 0);
    historyDeta.pop();
};
