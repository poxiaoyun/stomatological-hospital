package com.hospital.handler;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.beans.RemovableDentureOrder;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.OrderStatus;
import com.hospital.enumerate.UserRole;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.map.RemovableDentureMapElement;
import com.hospital.service.RemovableDentureFlowSchemeService;
import com.hospital.service.RemovableDentureOrderService;
import com.hospital.service.UserInformationService;
import com.hospital.utils.PagingUtils;
import com.hospital.utils.ShiroUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author shkstart
 * @date 2021-08-20 13:26
 */
@Slf4j
@Controller
public class RemovableDentureOrderHandler {

    private final RemovableDentureOrderService removableDentureOrderService;

    private final RemovableDentureFlowSchemeService removableDentureFlowSchemeService;

    private final UserInformationService userInformationService;

    public RemovableDentureOrderHandler(RemovableDentureOrderService removableDentureOrderService, RemovableDentureFlowSchemeService removableDentureFlowSchemeService, UserInformationService userInformationService) {
        this.removableDentureOrderService = removableDentureOrderService;
        this.removableDentureFlowSchemeService = removableDentureFlowSchemeService;
        this.userInformationService = userInformationService;
    }

    /**
     * 分页 查询活动义齿订单
     */
    @GetMapping("/table_removable")
    public String toTableRemovablePage(@RequestParam(value = "pn", defaultValue = "1") Integer current,
                                       @RequestParam(value = "username", defaultValue = "") String username,
                                       @RequestParam(value = "status", defaultValue = "0") Integer orderStatus,
                                       Model model){

        //获取活动义齿订单
        Page<RemovableDentureOrder> orderPaging = removableDentureOrderService.getOrderPaging(current, username, orderStatus);

        //页码集合
        model.addAttribute("pagination", PagingUtils.paginationShow(orderPaging));
        //获取所有医生
        List<UserInformation> doctors = userInformationService.getUserList(UserRole.HOSPITAL);
        model.addAttribute("doctors", doctors);

        model.addAttribute("orderPaging", orderPaging);
        model.addAttribute("orderType", "活动义齿");
        //添加活动义齿页面的url
        model.addAttribute("src", "/add_removable");
        //到活动义齿列表页面的url
        model.addAttribute("src2", "/table_removable");
        //到活动义齿详情页面的url
        model.addAttribute("src3", "/details_removable");
        //订单状态（搜索）
        model.addAttribute("status", orderStatus);
        //医生id（搜索）
        model.addAttribute("username", username);
        return "tables";
    }

    /**
     * 到添加活动义齿页面
     */
    @GetMapping("/add_removable")
    @RequiresRoles("HOSPITAL")
    public String toAddRemovableOrderPage(Model model){

        // 当前用户信息
        model.addAttribute("currentUserInformation", ShiroUtils.getCurrentUserInformation());
        //获取所以医生
        List<UserInformation> doctors = userInformationService.getUserList(UserRole.HOSPITAL);
        model.addAttribute("doctors", doctors);
        //项目
        model.addAttribute("items", RemovableDentureMapElement.getItems());
        //材料
        model.addAttribute("materials", RemovableDentureMapElement.getMaterials());
        return "add_removable";
    }

    /**
     * 添加活动义齿
     */
    @PostMapping("/add_removable")
    @RequiresRoles("HOSPITAL")
    public String addRemovableOrder(RemovableDentureOrder removableDentureOrder,
                                    @RequestPart("file")MultipartFile modelFile) throws IOException, RemindException, DataBaseException {
        removableDentureOrderService.addOrder(removableDentureOrder, modelFile);
        return "redirect:/table_removable";
    }

    /**
     * 到指定 活动义齿的详情页面
     */
    @GetMapping("/details_removable")
    public String toDetailsPage(@RequestParam("id") Integer id,
                                Model model) throws RemindException {

        RemovableDentureOrder removableDentureOrder = removableDentureOrderService.getParticularOrderById(id);
        List<RemovableDentureFlowScheme> removableDentureFlowSchemes = null;

        if (!removableDentureOrder.getStatus().equals(OrderStatus.UNFINISHED.getState())) {
            //该订单已经接单或已经结束 存在义齿制作流程单
            //..........查询义齿流程单
            removableDentureFlowSchemes = removableDentureFlowSchemeService.getFlowSchemeByOrderId(id);
        }

        //订单基本信息
        model.addAttribute("removableDentureOrder", removableDentureOrder);
        //订单的义齿制作流程表
        model.addAttribute("removableDentureFlowSchemes", removableDentureFlowSchemes);
        //固定义齿制作流程
        model.addAttribute("process", RemovableDentureMapElement.getProcess());
        //技师
        model.addAttribute("technicians", userInformationService.getUserList(UserRole.PLANT));

        return "details_removable";
    }

}
