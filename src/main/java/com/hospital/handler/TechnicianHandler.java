package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.*;
import com.hospital.enumerate.UserRole;
import com.hospital.exception.DataBaseException;
import com.hospital.service.*;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shkstart
 * @date 2021-08-27 20:42
 *
 * 技师
 */
@Controller
public class TechnicianHandler {

    private final UserInformationService userInformationService;

    private final RemovableDentureOrderService removableDentureOrderService;

    private final RemovableDentureFlowSchemeService removableDentureFlowSchemeService;

    private final FixedDentureOrderService fixedDentureOrderService;

    private final FixedDentureFlowSchemeService fixedDentureFlowSchemeService;

    public TechnicianHandler(UserInformationService userInformationService, RemovableDentureOrderService removableDentureOrderService, RemovableDentureFlowSchemeService removableDentureFlowSchemeService, FixedDentureOrderService fixedDentureOrderService, FixedDentureFlowSchemeService fixedDentureFlowSchemeService) {
        this.userInformationService = userInformationService;
        this.removableDentureOrderService = removableDentureOrderService;
        this.removableDentureFlowSchemeService = removableDentureFlowSchemeService;
        this.fixedDentureOrderService = fixedDentureOrderService;
        this.fixedDentureFlowSchemeService = fixedDentureFlowSchemeService;
    }

    @GetMapping("/technician")
    @RequiresRoles("PLANT")
    public String toTechnicianPage(@RequestParam(value = "date", defaultValue = "-1") String date,
                                   Model model){

        if (date.equals("-1")){
            LocalDate lastMonthDate = LocalDate.now();//.minusMonths(1);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
            date = lastMonthDate.format(formatter);
        }
        //当月有订单的技师列表
        //所有技师
        List<UserInformation> technicians = userInformationService.getUserList(UserRole.PLANT);

        for (UserInformation technician : technicians) {
            String username = technician.getUsername();
            // 活动义齿订单
            List<Integer> removableOrders = removableDentureFlowSchemeService.list(new QueryWrapper<RemovableDentureFlowScheme>().select("DISTINCT order_id").eq("username", username)).stream().map(RemovableDentureFlowScheme::getOrderId).collect(Collectors.toList());
            List<RemovableDentureOrder> removableDentureOrders = new ArrayList<>();
            if (removableOrders.size() > 0) {
                removableDentureOrders = removableDentureOrderService.list(new QueryWrapper<RemovableDentureOrder>()
                        .select("id", "patient", "sex", "age", "charge", "order_receiving_date", "cost")
                        .likeRight("order_receiving_date", date).in("id", removableOrders));
            }


            technician.setRemovableDentureOrderList(removableDentureOrders);

            // 固定义齿订单
            List<Integer> fixedOrders = fixedDentureFlowSchemeService.list(new QueryWrapper<FixedDentureFlowScheme>().select("DISTINCT order_id").eq("username", username)).stream().map(FixedDentureFlowScheme::getOrderId).collect(Collectors.toList());

            List<FixedDentureOrder> fixedDentureOrders = new ArrayList<>();
            if (fixedOrders.size() > 0) {
                fixedDentureOrders = fixedDentureOrderService.list(new QueryWrapper<FixedDentureOrder>()
                        .select("id", "patient", "sex", "age", "charge", "order_receiving_date", "cost")
                        .likeRight("order_receiving_date", date).in("id", fixedOrders));
            }

            technician.setFixedDentureOrderList(fixedDentureOrders);
        }

        model.addAttribute("technicians", technicians);
        model.addAttribute("date", date);
        return "technician";
    }
}
