package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.FixedDentureFlowScheme;
import com.hospital.beans.FixedDentureOrder;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.service.FixedDentureFlowSchemeService;
import com.hospital.service.FixedDentureOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author shkstart
 * @date 2021-08-28 17:28
 */
@Slf4j
@Controller
public class FixedDentureFlowSchemeHandler {

    private final FixedDentureFlowSchemeService fixedDentureFlowSchemeService;

    private final FixedDentureOrderService fixedDentureOrderService;

    public FixedDentureFlowSchemeHandler(FixedDentureFlowSchemeService fixedDentureFlowSchemeService, FixedDentureOrderService fixedDentureOrderService) {
        this.fixedDentureFlowSchemeService = fixedDentureFlowSchemeService;
        this.fixedDentureOrderService = fixedDentureOrderService;
    }

    /**
     * 工厂接单
     */
    @PostMapping("/orderReceivingFlow")
    @RequiresRoles("PLANT")
    public String orderReceiving(@RequestParam("id") Integer orderId,
                                 @RequestParam("allocation") String allocation,
                                 RedirectAttributes redirectAttributes) throws RemindException, DataBaseException {

        fixedDentureOrderService.orderAllocation(orderId, allocation);
        fixedDentureFlowSchemeService.orderReceiving(orderId);
        redirectAttributes.addAttribute("id", orderId);
        return "redirect:/details_fixed";
    }

    /**
     * 一道工序完成提交
     */
    @PostMapping("/processAccomplishFixed")
    @RequiresRoles("PLANT")
    public String processAccomplish(@RequestParam("orderId") Integer orderId,
                                    FixedDentureFlowScheme fixedDentureFlowScheme,
                                    RedirectAttributes redirectAttributes) throws RemindException, DataBaseException {

        if (fixedDentureFlowScheme.getUsername() == null) {
            processFixedDentureFlowScheme(fixedDentureFlowScheme, orderId);
        }

        fixedDentureFlowSchemeService.processAccomplish(fixedDentureFlowScheme);
        redirectAttributes.addAttribute("id", orderId);
        return "redirect:/details_fixed";
    }

    @PostMapping("/processAccomplishSkipFixed")
    @RequiresRoles("PLANT")
    public String processSkip(@RequestParam("orderId") Integer orderId,
                              FixedDentureFlowScheme fixedDentureFlowScheme,
                              RedirectAttributes redirectAttributes) throws RemindException, DataBaseException {

        if (fixedDentureFlowScheme.getUsername() == null) {
            processFixedDentureFlowScheme(fixedDentureFlowScheme, orderId);
        }

        fixedDentureFlowSchemeService.processAccomplishSkip(fixedDentureFlowScheme);
        redirectAttributes.addAttribute("id", orderId);
        return "redirect:/details_fixed";
    }

    /**
     * 加工 FixedDentureFlowScheme
     * @param fixedDentureFlowScheme
     * @param orderId
     * @return
     */
    private void processFixedDentureFlowScheme(FixedDentureFlowScheme fixedDentureFlowScheme, Integer orderId) {
        Integer processId = fixedDentureFlowScheme.getProcessId();
        FixedDentureFlowScheme one = fixedDentureFlowSchemeService.getOne(new QueryWrapper<FixedDentureFlowScheme>().eq("order_id", orderId).eq("process_id", processId - 1));
        fixedDentureFlowScheme.setUsername(one.getUsername());
        fixedDentureFlowScheme.setQuantity(one.getQuantity());
        fixedDentureFlowScheme.setUsernameSurveyor(one.getUsernameSurveyor());
    }

    /**
     * 问题记录
     */
    @PostMapping("/problemRecordFlow")
    @RequiresRoles("PLANT")
    public String problemRecord(@RequestParam("id") Integer id,
                                @RequestParam("problemRecord") String problemRecord,
                                RedirectAttributes redirectAttributes) throws DataBaseException {

        fixedDentureOrderService.problemRecord(id, problemRecord);

        redirectAttributes.addAttribute("id", id);
        return "redirect:/details_fixed";
    }

}
