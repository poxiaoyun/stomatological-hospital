package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.FixedDentureFlowScheme;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.service.RemovableDentureFlowSchemeService;
import com.hospital.service.RemovableDentureOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author shkstart
 * @date 2021-08-29 15:44
 */
@Slf4j
@Controller
public class RemovableDentureFlowSchemeHandler {

    private final RemovableDentureOrderService removableDentureOrderService;

    private final RemovableDentureFlowSchemeService removableDentureFlowSchemeService;

    public RemovableDentureFlowSchemeHandler(RemovableDentureOrderService removableDentureOrderService, RemovableDentureFlowSchemeService removableDentureFlowSchemeService) {
        this.removableDentureOrderService = removableDentureOrderService;
        this.removableDentureFlowSchemeService = removableDentureFlowSchemeService;
    }

    /**
     * 工厂接单
     */
    @PostMapping("/orderReceivingRemovable")
    @RequiresRoles("PLANT")
    public String orderReceiving(@RequestParam("id") Integer orderId,
                                 @RequestParam("allocation") String allocation,
                                 RedirectAttributes redirectAttributes) throws RemindException, DataBaseException {

        removableDentureOrderService.orderAllocation(orderId, allocation);
        removableDentureFlowSchemeService.orderReceiving(orderId);
        redirectAttributes.addAttribute("id", orderId);
        return "redirect:/details_removable";
    }

    /**
     * 一道工序完成提交
     */
    @PostMapping("/processAccomplishRemovable")
    @RequiresRoles("PLANT")
    public String processAccomplish(@RequestParam("orderId") Integer orderId,
                                    RemovableDentureFlowScheme removableDentureFlowScheme,
                                    RedirectAttributes redirectAttributes) throws RemindException, DataBaseException {

        if (removableDentureFlowScheme.getUsername() == null) {
            processRemovableDentureFlowScheme(removableDentureFlowScheme, orderId);
        }

        removableDentureFlowSchemeService.processAccomplish(removableDentureFlowScheme);
        redirectAttributes.addAttribute("id", orderId);
        return "redirect:/details_removable";
    }

    @PostMapping("/processAccomplishSkipRemovable")
    @RequiresRoles("PLANT")
    public String processSkip(@RequestParam("orderId") Integer orderId,
                              RemovableDentureFlowScheme removableDentureFlowScheme,
                              RedirectAttributes redirectAttributes) throws RemindException, DataBaseException {

        if (removableDentureFlowScheme.getUsername() == null) {
            processRemovableDentureFlowScheme(removableDentureFlowScheme, orderId);
        }

        removableDentureFlowSchemeService.processAccomplishSkip(removableDentureFlowScheme);
        redirectAttributes.addAttribute("id", orderId);
        return "redirect:/details_removable";
    }

    /**
     * 加工 RemovableDentureFlowScheme
     * @param removableDentureFlowScheme
     * @return
     */
    private void processRemovableDentureFlowScheme(RemovableDentureFlowScheme removableDentureFlowScheme, Integer orderId) {
        Integer processId = removableDentureFlowScheme.getProcessId();
        RemovableDentureFlowScheme one = removableDentureFlowSchemeService.getOne(new QueryWrapper<RemovableDentureFlowScheme>().eq("order_id", orderId).eq("process_id", processId - 1));
        removableDentureFlowScheme.setUsername(one.getUsername());
        removableDentureFlowScheme.setQuantity(one.getQuantity());
        removableDentureFlowScheme.setUsernameSurveyor(one.getUsernameSurveyor());
    }

    /**
     * 问题记录
     */
    @PostMapping("/problemRecordRemovable")
    @RequiresRoles("PLANT")
    public String problemRecord(@RequestParam("id") Integer id,
                                @RequestParam("problemRecord") String problemRecord,
                                RedirectAttributes redirectAttributes) throws DataBaseException {

        removableDentureOrderService.problemRecord(id, problemRecord);

        redirectAttributes.addAttribute("id", id);
        return "redirect:/details_removable";
    }
}
