package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.FixedDentureOrder;
import com.hospital.beans.RemovableDentureOrder;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.UserRole;
import com.hospital.service.FixedDentureOrderService;
import com.hospital.service.RemovableDentureOrderService;
import com.hospital.service.UserInformationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author shkstart
 * @date 2021-08-26 21:41
 */
@Slf4j
@Controller
public class DoctorHandler {

    private final UserInformationService userInformationService;

    private final RemovableDentureOrderService removableDentureOrderService;

    private final FixedDentureOrderService fixedDentureOrderService;

    public DoctorHandler(UserInformationService userInformationService, RemovableDentureOrderService removableDentureOrderService, FixedDentureOrderService fixedDentureOrderService) {
        this.userInformationService = userInformationService;
        this.removableDentureOrderService = removableDentureOrderService;
        this.fixedDentureOrderService = fixedDentureOrderService;
    }

    @GetMapping("/doctor")
    @RequiresRoles("HOSPITAL")
    public String toDoctorPage(@RequestParam(value = "date", defaultValue = "-1") String date,
                               Model model){

        if (date.equals("-1")){
            LocalDate lastMonthDate = LocalDate.now();//.minusMonths(1);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
            date = lastMonthDate.format(formatter);
        }

        //当月有订单的医生列表
        //所有医生
        List<UserInformation> doctors = userInformationService.getUserList(UserRole.HOSPITAL);

        for (UserInformation doctor : doctors) {
            String username = doctor.getUsername();
            // 活动义齿
            List<RemovableDentureOrder> removableDentureOrders = removableDentureOrderService.list(new QueryWrapper<RemovableDentureOrder>()
                    .select("id", "patient", "sex", "age", "charge", "order_receiving_date", "cost")
                    .eq("username", username)
                    .likeRight("order_receiving_date", date));

            doctor.setRemovableDentureOrderList(removableDentureOrders);

            // 固定义齿
            List<FixedDentureOrder> fixedDentureOrders = fixedDentureOrderService.list(new QueryWrapper<FixedDentureOrder>()
                    .select("id", "patient", "sex", "age", "charge", "order_receiving_date", "cost")
                    .eq("username", username)
                    .likeRight("order_receiving_date", date));

            doctor.setFixedDentureOrderList(fixedDentureOrders);
        }

        model.addAttribute("doctors", doctors);
        model.addAttribute("date", date);
        return "doctor";
    }
}
