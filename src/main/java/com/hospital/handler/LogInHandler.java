package com.hospital.handler;

import com.hospital.beans.UserInformation;
import com.hospital.enumerate.UserRole;
import com.hospital.service.UserInformationService;
import com.hospital.utils.ShiroUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author shkstart
 * @date 2021-08-19 19:20
 */
@Slf4j
@Controller
public class LogInHandler {

    private final UserInformationService userInformationService;

    public LogInHandler(UserInformationService userInformationService) {
        this.userInformationService = userInformationService;
    }

    @GetMapping(value = {"/","/login"})
    public String toLogInPage(){
        return "login";
    }

    @PostMapping("/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password){

        log.info("登录：账号{}     密码{}", username, password);

        //获取Subject对象
        Subject subject = SecurityUtils.getSubject();
        //如果用户未注销，先注销再登录
        if (subject.isAuthenticated()) {
            log.info("用户注销：{}", ShiroUtils.getCurrentUserInformation());
            subject.logout();
        }
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
//        token.setRememberMe(true);
        subject.login(token);
        // 登录成功

        UserInformation userInformation = ShiroUtils.getCurrentUserInformation();
        userInformation.setPassword(null);
        //判断当前登录的用户角色 医生 技师
        if(userInformation.getRoleId().equals(UserRole.HOSPITAL.getState())){
            return "redirect:/index";
        }
        if(userInformation.getRoleId().equals(UserRole.PLANT.getState())){
            return "redirect:/indexs";
        }
        //如果是超管，则默认登录医生页面
        return "redirect:/index";
    }

}
