package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.FixedDentureOrder;
import com.hospital.beans.RemovableDentureOrder;
import com.hospital.enumerate.OrderStatus;
import com.hospital.service.FixedDentureOrderService;
import com.hospital.service.RemovableDentureOrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author shkstart
 * @date 2021-08-19 20:06
 */
@Slf4j
@Controller
public class IndexHandler {

    private final FixedDentureOrderService fixedDentureOrderService;

    private final RemovableDentureOrderService removableDentureOrderService;

    public IndexHandler(FixedDentureOrderService fixedDentureOrderService, RemovableDentureOrderService removableDentureOrderService) {
        this.fixedDentureOrderService = fixedDentureOrderService;
        this.removableDentureOrderService = removableDentureOrderService;
    }

    /**
     * 医院首页
     */
    @GetMapping("/index")
    @RequiresRoles("HOSPITAL")
    public String toIndexPage(Model model){

        //活动义齿总数
        model.addAttribute("removableAllCount", removableDentureOrderService.count());
        model.addAttribute("removableNotFinishCount", removableDentureOrderService.count(new QueryWrapper<RemovableDentureOrder>().ne("status", OrderStatus.FINISH.getState())));

        //固定义齿总数
        model.addAttribute("fixedAllCount", fixedDentureOrderService.count());
        model.addAttribute("fixedNotFinishCount", fixedDentureOrderService.count(new QueryWrapper<FixedDentureOrder>().ne("status", OrderStatus.FINISH.getState())));


        return "index";
    }

    /**
     * 工厂首页
     */
    @GetMapping("/indexs")
    @RequiresRoles("PLANT")
    public String toIndexPlantPage(Model model){

        //活动义齿总数
        model.addAttribute("removableAllCount", removableDentureOrderService.count());
        model.addAttribute("removableUnfinishedCount", removableDentureOrderService.count(new QueryWrapper<RemovableDentureOrder>().eq("status", OrderStatus.UNFINISHED.getState())));

        //固定义齿总数
        model.addAttribute("fixedAllCount", fixedDentureOrderService.count());
        model.addAttribute("fixedUnfinishedCount", fixedDentureOrderService.count(new QueryWrapper<FixedDentureOrder>().eq("status", OrderStatus.UNFINISHED.getState())));
        return "index_plant";
    }

}
