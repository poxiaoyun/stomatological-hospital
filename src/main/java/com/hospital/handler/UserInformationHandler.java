package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.hospital.beans.UserInformation;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.service.UserInformationService;
import com.hospital.utils.ShiroUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 破晓
 * @date 2022-01-28 15:27
 */
@Controller
public class UserInformationHandler {

    @Autowired
    private UserInformationService userInformationService;

    @GetMapping("/user_information")
    public String toUserInformationPage(Model model) {
        UserInformation currentUserInformation = ShiroUtils.getCurrentUserInformation();
        model.addAttribute("userInformation", currentUserInformation);
        return "user_information";
    }

    @PostMapping("/update_password")
    public String updatePassword(String oldPassword, String password) throws RemindException, DataBaseException {
        Integer id = ShiroUtils.getCurrentUserInformation().getId();
        UserInformation userInformation = userInformationService.getOne(new QueryWrapper<UserInformation>().select("password").eq("id", id));
        // 密码不正确
        if (!userInformation.getPassword().equals(oldPassword)) {
            throw new RemindException("当前密码错误");
        }
        boolean update = userInformationService.update(new UpdateWrapper<UserInformation>().set("password", password).eq("id", id));
        if (!update) {
            throw new DataBaseException();
        }
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/";
    }

    /**
     * 添加用户信息
     * @param userInformation
     * @return
     * @throws RemindException
     * @throws DataBaseException
     */
    @PostMapping("/add_user")
    @RequiresRoles("ADMIN")
    public String addUserInformation(UserInformation userInformation) throws RemindException, DataBaseException {
        userInformationService.saveUserInformation(userInformation);
        return "redirect:/admin";
    }

    @PostMapping("/update_user")
    @RequiresRoles("ADMIN")
    public String updateUserInformation(UserInformation userInformation) throws DataBaseException {
        if (!userInformationService.updateById(userInformation)) {
            throw new DataBaseException();
        }
        return "redirect:/admin";
    }

    @PostMapping("/delete_user")
    @RequiresRoles("ADMIN")
    public String deleteUserInformation(@RequestParam("delete_username") String username,
                                        @RequestParam("delete_password") String password) throws RemindException, DataBaseException {
        // 验证密码正确性
        if (!userInformationService.verifyPassword(password)) {
            throw new RemindException("密码错误");
        }
        userInformationService.removeUserInformation(username);
        return "redirect:/admin";
    }
}
