package com.hospital.handler;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.UserRole;
import com.hospital.service.UserInformationService;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author 破晓
 * @date 2022-01-29 12:49
 */
@Controller
public class AdminHandler {

    private final UserInformationService userInformationService;

    public AdminHandler(UserInformationService userInformationService) {
        this.userInformationService = userInformationService;
    }

    @GetMapping("/admin")
    @RequiresRoles("ADMIN")
    public String toAdminPage(Model model) {
        List<UserInformation> userInformations = userInformationService.list(new QueryWrapper<UserInformation>().select("id", "username", "name", "division", "role_id", "state").ne("role_id", UserRole.SUPER_TUBE.getState()));
        model.addAttribute("userInformations", userInformations);
        return "admin";
    }
}
