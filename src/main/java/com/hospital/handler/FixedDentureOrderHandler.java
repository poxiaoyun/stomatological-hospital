package com.hospital.handler;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hospital.beans.FixedDentureFlowScheme;
import com.hospital.beans.FixedDentureOrder;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.OrderStatus;
import com.hospital.enumerate.UserRole;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.map.FixedDentureMapElement;
import com.hospital.modules.ModelFileIO;
import com.hospital.service.FixedDentureFlowSchemeService;
import com.hospital.service.FixedDentureOrderService;
import com.hospital.service.UserInformationService;
import com.hospital.utils.PagingUtils;
import com.hospital.utils.ShiroUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shkstart
 * @date 2021-08-26 21:42
 */
@Slf4j
@Controller
public class FixedDentureOrderHandler {

    private final FixedDentureOrderService fixedDentureOrderService;

    private final FixedDentureFlowSchemeService fixedDentureFlowSchemeService;

    private final UserInformationService userInformationService;

    public FixedDentureOrderHandler(FixedDentureOrderService fixedDentureOrderService, FixedDentureFlowSchemeService fixedDentureFlowSchemeService, UserInformationService userInformationService) {
        this.fixedDentureOrderService = fixedDentureOrderService;
        this.fixedDentureFlowSchemeService = fixedDentureFlowSchemeService;
        this.userInformationService = userInformationService;
    }

    /**
     * 分页 查询固定义齿订单
     */
    @GetMapping("/table_fixed")
    public String toTableFixedPage(@RequestParam(value = "pn", defaultValue = "1") Integer current,
                                   @RequestParam(value = "username", defaultValue = "") String username,
                                   @RequestParam(value = "status", defaultValue = "0") Integer orderStatus,
                                   Model model){

        //获取固定义齿订单
        Page<FixedDentureOrder> orderPaging = fixedDentureOrderService.getOrderPaging(current, username, orderStatus);

        //页码集合
        model.addAttribute("pagination", PagingUtils.paginationShow(orderPaging));
        //获取所以医生
        List<UserInformation> doctors = userInformationService.getUserList(UserRole.HOSPITAL);
        model.addAttribute("doctors", doctors);

        model.addAttribute("orderPaging", orderPaging);
        model.addAttribute("orderType", "固定义齿");
        //添加固定义齿页面的url
        model.addAttribute("src", "/add_fixed");
        //到固定义齿列表页面的url
        model.addAttribute("src2", "/table_fixed");
        //到固定义齿详情页面的url
        model.addAttribute("src3", "/details_fixed");
        //订单状态（搜索）
        model.addAttribute("status", orderStatus);
        //医生id（搜索）
        model.addAttribute("username", username);
        return "tables";
    }

    /**
     * 到添加固定义齿订单页面
     */
    @GetMapping("/add_fixed")
    @RequiresRoles("HOSPITAL")
    public String toAddFixedOrderPage(Model model){

        // 当前用户信息
        model.addAttribute("currentUserInformation", ShiroUtils.getCurrentUserInformation());
        //获取所以医生
        List<UserInformation> doctors = userInformationService.getUserList(UserRole.HOSPITAL);
        model.addAttribute("doctors", doctors);
        //项目
        model.addAttribute("items", FixedDentureMapElement.getItems());
        //材料
        model.addAttribute("materials", FixedDentureMapElement.getMaterials());
        //烤瓷冠设计要求
        model.addAttribute("baked_porcelain_crown", FixedDentureMapElement.getBaked_porcelain_crown());
        //桥体设计
        model.addAttribute("the_bridge_design", FixedDentureMapElement.getThe_bridge_design());
        //邻间隙
        model.addAttribute("interproximal_clearance", FixedDentureMapElement.getInterproximal_clearance());
        //咬合关系
        model.addAttribute("occluding_relation", FixedDentureMapElement.getOccluding_relation());
        //邻接关系
        model.addAttribute("syntopy", FixedDentureMapElement.getSyntopy());
        return "add_fixed";
    }

    /**
     * 添加固定义齿订单
     */
    @PostMapping("/add_fixed")
    @RequiresRoles("HOSPITAL")
    public String addFixedOrder(FixedDentureOrder fixedDentureOrder,
                                @RequestPart("file") MultipartFile modelFile) throws IOException, RemindException, DataBaseException {

        fixedDentureOrderService.addOrder(fixedDentureOrder, modelFile);

        return "redirect:/table_fixed";
    }

    /**
     * 到指定 固定义齿的详情页面
     */
    @GetMapping("/details_fixed")
    public String toDetailsPage(@RequestParam("id") Integer id,
                                Model model) throws RemindException {

        FixedDentureOrder fixedDentureOrder = fixedDentureOrderService.getParticularOrderById(id);
        List<FixedDentureFlowScheme> fixedDentureFlowSchemes = null;

        if (!fixedDentureOrder.getStatus().equals(OrderStatus.UNFINISHED.getState())) {
            //该订单已经接单或已经结束 存在义齿制作流程单
            //..........查询义齿流程单
            fixedDentureFlowSchemes = fixedDentureFlowSchemeService.getFlowSchemeByOrderId(id);
        }

        //订单基本信息
        model.addAttribute("fixedDentureOrder", fixedDentureOrder);
        //订单的义齿制作流程表
        model.addAttribute("fixedDentureFlowSchemes", fixedDentureFlowSchemes);
        //固定义齿制作流程
        model.addAttribute("process", FixedDentureMapElement.getProcess());
        //技师
        model.addAttribute("technicians", userInformationService.getUserList(UserRole.PLANT));

        return "details_fixed";
    }

    @GetMapping("/download")
    @ResponseBody
    public String modelFileDownload(@RequestParam("id") String fileName,
                                    HttpServletResponse httpServletResponse) throws RemindException, IOException {
        log.info("下载");
        new ModelFileIO().download(fileName, httpServletResponse);
        return "";
    }

}
