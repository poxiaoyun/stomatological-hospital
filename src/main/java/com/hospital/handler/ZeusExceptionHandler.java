package com.hospital.handler;

import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author shkstart
 * @date 2021-08-31 11:30
 */
@ControllerAdvice
public class ZeusExceptionHandler {

    @ExceptionHandler({UnknownAccountException.class, IncorrectCredentialsException.class})
    public ModelAndView exceptionHandler(Exception e){
        String message = e.getMessage();
        if(e instanceof IncorrectCredentialsException){
            message = "密码错误";
        }
        ModelAndView mv = new ModelAndView();
        mv.addObject("message", message);
        mv.setViewName("login");
        return mv;
    }
}
