package com.hospital.exception;

/**
 * @author shkstart
 * @date 2021-08-17 17:16
 */
public class DataBaseException extends Exception {

    public DataBaseException() {
        super("数据库操作异常");
    }

    public DataBaseException(String message) {
        super(message);
    }
}
