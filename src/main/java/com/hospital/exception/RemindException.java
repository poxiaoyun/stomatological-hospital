package com.hospital.exception;

/**
 * @author shkstart
 * @date 2021-08-18 16:39
 */
public class RemindException extends Exception {

    public RemindException(String message) {
        super(message);
    }
}
