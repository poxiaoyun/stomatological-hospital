package com.hospital.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.beans.UserInformation;
import com.hospital.config.HospitalProperties;
import com.hospital.enumerate.OrderStatus;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.mapper.RemovableDentureFlowSchemeMapper;
import com.hospital.mapper.RemovableDentureOrderMapper;
import com.hospital.beans.RemovableDentureOrder;
import com.hospital.mapper.UserInformationMapper;
import com.hospital.modules.CanvasImageFileIO;
import com.hospital.modules.ModelFileIO;
import com.hospital.service.RemovableDentureOrderService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * (RemovableDentureOrder)表服务实现类
 *
 * @author 破晓
 * @since 2021-08-16 21:33:26
 */
@Service
@EnableConfigurationProperties({HospitalProperties.class})
public class RemovableDentureOrderServiceImpl extends ServiceImpl<RemovableDentureOrderMapper, RemovableDentureOrder> implements RemovableDentureOrderService {

    private final HospitalProperties hospitalProperties;

    private final RemovableDentureOrderMapper removableDentureOrderMapper;

    private final RemovableDentureFlowSchemeMapper removableDentureFlowSchemeMapper;

    private final UserInformationMapper userInformationMapper;

    private Integer PAGE_SIZE;

    public RemovableDentureOrderServiceImpl(HospitalProperties hospitalProperties, RemovableDentureOrderMapper removableDentureOrderMapper, RemovableDentureFlowSchemeMapper removableDentureFlowSchemeMapper, UserInformationMapper userInformationMapper) {
        this.hospitalProperties = hospitalProperties;
        this.removableDentureOrderMapper = removableDentureOrderMapper;
        this.removableDentureFlowSchemeMapper = removableDentureFlowSchemeMapper;

        this.PAGE_SIZE = hospitalProperties.getRemovable_denture_order_display_size();
        this.userInformationMapper = userInformationMapper;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addOrder(RemovableDentureOrder removableDentureOrder, MultipartFile multipartFile) throws DataBaseException, RemindException {

        //新增订单，订单状态设置为 未接单
        removableDentureOrder.setStatus(OrderStatus.UNFINISHED.getState());

        //保存义齿画布base64文件
        CanvasImageFileIO canvasImageFileIO = new CanvasImageFileIO();
        removableDentureOrder.setCanvasImageId(canvasImageFileIO.uploading(removableDentureOrder.getCanvasImage()));

        //如果文件不为空，则上传文件
        ModelFileIO modelFileIO = new ModelFileIO();
        String fileName = modelFileIO.uploading(multipartFile);
        removableDentureOrder.setModelId(fileName);

        if (removableDentureOrderMapper.insert(removableDentureOrder) < 1) {
            throw new DataBaseException();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeOrder(Integer id) throws DataBaseException {

        //确保订单和流程检验表一并删除
        Integer count = removableDentureFlowSchemeMapper.selectCount(new QueryWrapper<RemovableDentureFlowScheme>().eq("order_id", id));
        if (count > 0) {
            if (removableDentureFlowSchemeMapper.delete(new QueryWrapper<RemovableDentureFlowScheme>().eq("order_id", id)) < 1) {
                throw new DataBaseException();
            }
        }

        if (removableDentureOrderMapper.deleteById(id) < 1) {
            throw new DataBaseException();
        }
    }

    @Override
    public Page<RemovableDentureOrder> getOrderPaging(Integer current, String doctorUsername, Integer status) {

        boolean bStatus = true;
        boolean bDoctorId = true;
        if (status.equals(OrderStatus.NULL.getState())) {
            bStatus = false;
        }

        if ("".equals(doctorUsername)) {
            bDoctorId = false;
        }

        Page<RemovableDentureOrder> page = removableDentureOrderMapper.selectPage(new Page<RemovableDentureOrder>(current, PAGE_SIZE), new QueryWrapper<RemovableDentureOrder>()
                .select("id", "patient", "sex", "age", "username", "charge", "order_receiving_date", "delivery_date", "status")
                .eq(bStatus, "status", status)
                .eq(bDoctorId, "username", doctorUsername)
                .orderByDesc("order_receiving_date")
                .orderByDesc("id"));

        for (RemovableDentureOrder order : page.getRecords()) {
            //将doctorId 转为 doctorName
            order.setDoctorName(usernameToName(order.getUsername()));
            //将订单状态码转为字符串
            order.setStatusName(OrderStatus.getOrderStatusByState(order.getStatus()).getInfo());
        }

        return page;
    }

    @Override
    public List<RemovableDentureOrder> getOrderSearch(String doctorUsername, String patient) {

        List<RemovableDentureOrder> list = removableDentureOrderMapper.selectList(new QueryWrapper<RemovableDentureOrder>()
                .select("id", "patient", "sex", "age", "username", "charge", "order_receiving_date", "delivery_date", "status")
                .eq("username", doctorUsername)
                .like("patient", patient.trim())
                .orderByDesc("order_receiving_date")
                .orderByDesc("id"));

        //将doctorId 转为 doctorName
        for (RemovableDentureOrder order : list) {
            order.setDoctorName(usernameToName(order.getUsername()));
        }
        return list;
    }

    @Override
    public RemovableDentureOrder getParticularOrderById(Integer id) throws RemindException {

        // 查询订单信息
        RemovableDentureOrder order = removableDentureOrderMapper.selectById(id);

        //
        String canvasImageId = order.getCanvasImageId();
        CanvasImageFileIO canvasImageFileIO = new CanvasImageFileIO();
        order.setCanvasImage(canvasImageFileIO.getCanvasImage(canvasImageId));
        order.setDoctorName(usernameToName(order.getUsername()));
        return order;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderAllocation(Integer id, String allocation) throws DataBaseException {
        RemovableDentureOrder removableDentureOrder = new RemovableDentureOrder();
        removableDentureOrder.setId(id);
        removableDentureOrder.setAllocation(allocation);
        if (removableDentureOrderMapper.updateById(removableDentureOrder) < 1) {
            throw new DataBaseException();
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void problemRecord(Integer id, String problemRecord) throws DataBaseException {
        RemovableDentureOrder removableDentureOrder = new RemovableDentureOrder();
        removableDentureOrder.setId(id);
        removableDentureOrder.setProblemRecord(problemRecord);
        if (removableDentureOrderMapper.updateById(removableDentureOrder) < 1) {
            throw new DataBaseException();
        }
    }

    /**
     * 根据医生的用户名获取医生的姓名
     */
    private String usernameToName(String username) {
        UserInformation userInformation = userInformationMapper.selectOne(new QueryWrapper<UserInformation>().eq("username", username));
        return userInformation.getName();
    }
}
