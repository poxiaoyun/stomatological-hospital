package com.hospital.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospital.beans.RemovableDentureOrder;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.OrderStatus;
import com.hospital.enumerate.Sign;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.map.RemovableDentureMapElement;
import com.hospital.mapper.RemovableDentureFlowSchemeMapper;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.mapper.RemovableDentureOrderMapper;
import com.hospital.mapper.UserInformationMapper;
import com.hospital.service.RemovableDentureFlowSchemeService;
import com.hospital.service.UserInformationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * (RemovableDentureFlowScheme)表服务实现类
 *
 * @author 破晓
 * @since 2021-08-16 21:32:22
 */
@Service
public class RemovableDentureFlowSchemeServiceImpl extends ServiceImpl<RemovableDentureFlowSchemeMapper, RemovableDentureFlowScheme> implements RemovableDentureFlowSchemeService {

    private final RemovableDentureOrderMapper removableDentureOrderMapper;

    private final RemovableDentureFlowSchemeMapper removableDentureFlowSchemeMapper;

    private final UserInformationMapper userInformationMapper;

    public RemovableDentureFlowSchemeServiceImpl(RemovableDentureOrderMapper removableDentureOrderMapper, RemovableDentureFlowSchemeMapper removableDentureFlowSchemeMapper, UserInformationMapper userInformationMapper) {
        this.removableDentureOrderMapper = removableDentureOrderMapper;
        this.removableDentureFlowSchemeMapper = removableDentureFlowSchemeMapper;
        this.userInformationMapper = userInformationMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderReceiving(Integer orderId) throws DataBaseException, RemindException {

        //检验订单是否为为未接单状态
        RemovableDentureOrder removableDentureOrder = removableDentureOrderMapper.selectOne(new QueryWrapper<RemovableDentureOrder>()
                .select("id", "status")
                .eq("id", orderId));
        if (!removableDentureOrder.getStatus().equals(OrderStatus.UNFINISHED.getState())) {
            throw new RemindException("订单已经接收或已经结束");
        }

        //接单后，将订单的状态改为已接单
        removableDentureOrder.setStatus(OrderStatus.UNDERWAY.getState());
        if (removableDentureOrderMapper.updateById(removableDentureOrder) < 1) {
            throw new DataBaseException();
        }

        //创建流程检验表格
        Map<Integer, String > process = RemovableDentureMapElement.getProcess();

        //删除该订单的所有流程
        removableDentureFlowSchemeMapper.delete(new QueryWrapper<RemovableDentureFlowScheme>().eq("order_id", orderId));

        for (Map.Entry<Integer, String> entry : process.entrySet()) {
            RemovableDentureFlowScheme removableDentureFlowScheme = new RemovableDentureFlowScheme();
            removableDentureFlowScheme.setOrderId(orderId);
            removableDentureFlowScheme.setProcessId(entry.getKey());
            //为工序添加标记,如果当前工序是第一道工序时，将标记改为 等待完成
            if(entry.getKey().equals(1)){
                removableDentureFlowScheme.setSign(Sign.UNDERWAY.getState());
            }else {
                removableDentureFlowScheme.setSign(Sign.FINISH.getState());
            }
            if (removableDentureFlowSchemeMapper.insert(removableDentureFlowScheme) < 1) {
                throw new DataBaseException();
            }
        }
        //垃圾回收
        System.gc();

    }

    @Override
    public void processAccomplish(RemovableDentureFlowScheme removableDentureFlowScheme) throws DataBaseException, RemindException {
        process(removableDentureFlowScheme, Sign.UNFINISHED.getState());
    }

    private void process(RemovableDentureFlowScheme removableDentureFlowScheme, Integer sign) throws DataBaseException, RemindException {

        Integer orderId = removableDentureFlowScheme.getOrderId();
        Integer processId = removableDentureFlowScheme.getProcessId();

        //检验订单是否已经接收
        RemovableDentureOrder removableDentureOrder = removableDentureOrderMapper.selectOne(new QueryWrapper<RemovableDentureOrder>().select("id", "status").eq("id", orderId));
        if (!removableDentureOrder.getStatus().equals(OrderStatus.UNDERWAY.getState())) {
            throw new RemindException("订单未接收或已经结束");
        }

        //为下一道工序设置标记 为 等待完成
        //如过当前工序不是最后一道工序时，为下一道工序设置标记
        if (processId != RemovableDentureMapElement.getProcess().size()){
            RemovableDentureFlowScheme removableDentureFlowScheme2 = new RemovableDentureFlowScheme();
            removableDentureFlowScheme2.setSign(Sign.UNDERWAY.getState());
            if (removableDentureFlowSchemeMapper.update(removableDentureFlowScheme2, new QueryWrapper<RemovableDentureFlowScheme>().eq("order_id", orderId).eq("process_id", processId + 1)) < 1) {
                throw new DataBaseException();
            }
        } else {//如果工序为 最后一道工序时，将订单的状态改为已结束
            //接单后，将订单的状态改为交付完工
            removableDentureOrder.setStatus(OrderStatus.FINISH.getState());
            if (removableDentureOrderMapper.updateById(removableDentureOrder) < 1) {
                throw new DataBaseException();
            }
        }

        //添加标记 当前工序已经完成
        removableDentureFlowScheme.setSign(sign);
        if (removableDentureFlowSchemeMapper.updateById(removableDentureFlowScheme) < 1) {
            throw new DataBaseException();
        }
    }

    @Override
    public void processAccomplishSkip(RemovableDentureFlowScheme removableDentureFlowScheme) throws RemindException, DataBaseException {
        removableDentureFlowScheme.setCompletionTime("");
        process(removableDentureFlowScheme, Sign.SKIP.getState());
    }

    @Override
    public List<RemovableDentureFlowScheme> getFlowSchemeByOrderId(Integer orderId) {

        List<RemovableDentureFlowScheme> removableDentureFlowSchemes = removableDentureFlowSchemeMapper.selectList(new QueryWrapper<RemovableDentureFlowScheme>()
                .select("id", "process_id", "username", "quantity", "completion_time", "username_surveyor", "sign")
                .eq("order_id", orderId)
                .orderByAsc("process_id"));

        Map<Integer, String> process = RemovableDentureMapElement.getProcess();

        for (RemovableDentureFlowScheme scheme : removableDentureFlowSchemes) {
            //将工序id转为工序名
            Integer processId = scheme.getProcessId();
            if (processId == null){
                scheme.setProcessName("");
            }else {
                scheme.setProcessName(process.get(processId));
            }
            //将技师id和技师检验人id转为对应的名字
            String username = scheme.getUsername();
            if (username == null){
                scheme.setTechnicianName("");
            }else {
                scheme.setTechnicianName(userInformationMapper.selectOne(new QueryWrapper<UserInformation>().eq("username", username)).getName());
            }
            String usernameSurveyor = scheme.getUsernameSurveyor();
            if (usernameSurveyor == null){
                scheme.setTechnicianSurveyorName("");
            }else {
                scheme.setTechnicianSurveyorName(userInformationMapper.selectOne(new QueryWrapper<UserInformation>().eq("username", usernameSurveyor)).getName());
            }
        }

        return removableDentureFlowSchemes;
    }

}
