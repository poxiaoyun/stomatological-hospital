package com.hospital.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospital.beans.*;
import com.hospital.config.HospitalProperties;
import com.hospital.enumerate.OrderStatus;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.mapper.FixedDentureFlowSchemeMapper;
import com.hospital.mapper.FixedDentureOrderMapper;
import com.hospital.mapper.UserInformationMapper;
import com.hospital.modules.CanvasImageFileIO;
import com.hospital.modules.ModelFileIO;
import com.hospital.service.FixedDentureOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * (FixedDentureOrder)表服务实现类
 *
 * @author makejava
 * @since 2021-08-21 13:46:33
 */
@Slf4j
@Service
public class FixedDentureOrderServiceImpl extends ServiceImpl<FixedDentureOrderMapper, FixedDentureOrder> implements FixedDentureOrderService {

    private final HospitalProperties hospitalProperties;

    private final FixedDentureOrderMapper fixedDentureOrderMapper;

    private final FixedDentureFlowSchemeMapper fixedDentureFlowSchemeMapper;

    private final UserInformationMapper userInformationMapper;

    private Integer PAGE_SIZE;

    public FixedDentureOrderServiceImpl(HospitalProperties hospitalProperties, FixedDentureOrderMapper fixedDentureOrderMapper, FixedDentureFlowSchemeMapper fixedDentureFlowSchemeMapper, UserInformationMapper userInformationMapper) {
        this.hospitalProperties = hospitalProperties;
        this.fixedDentureOrderMapper = fixedDentureOrderMapper;
        this.fixedDentureFlowSchemeMapper = fixedDentureFlowSchemeMapper;
        this.PAGE_SIZE = hospitalProperties.getRemovable_denture_order_display_size();
        this.userInformationMapper = userInformationMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addOrder(FixedDentureOrder fixedDentureOrder, MultipartFile multipartFile) throws DataBaseException, RemindException, IOException {

        //新增订单，订单状态设置为 未接单
        fixedDentureOrder.setStatus(OrderStatus.UNFINISHED.getState());

        //保存义齿画布base64文件
        CanvasImageFileIO canvasImageFileIO = new CanvasImageFileIO();
        fixedDentureOrder.setCanvasImageId(canvasImageFileIO.uploading(fixedDentureOrder.getCanvasImage()));

        //文件上传
        ModelFileIO modelFileIO = new ModelFileIO();
        String fileName = modelFileIO.uploading(multipartFile);
        fixedDentureOrder.setModelId(fileName);

        if (fixedDentureOrderMapper.insert(fixedDentureOrder) < 1) {
            throw new DataBaseException();
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeOrder(Integer id) throws DataBaseException {

        //确保订单和流程检验表一并删除，删除义齿制作相关流程
        Integer count = fixedDentureFlowSchemeMapper.selectCount(new QueryWrapper<FixedDentureFlowScheme>().eq("order_id", id));
        if (count > 0) {
            if (fixedDentureFlowSchemeMapper.delete(new QueryWrapper<FixedDentureFlowScheme>().eq("order_id", id)) < 1) {
                throw new DataBaseException();
            }
        }
        // 删除订单
        if (fixedDentureOrderMapper.deleteById(id) < 1) {
            throw new DataBaseException();
        }
    }

    @Override
    public Page<FixedDentureOrder> getOrderPaging(Integer current, String doctorUsername, Integer status) {

        boolean bStatus = true;
        boolean bDoctorId = true;
        if (status.equals(OrderStatus.NULL.getState())){
            bStatus = false;
        }

        if ("".equals(doctorUsername)){
            bDoctorId = false;
        }

        Page<FixedDentureOrder> page = fixedDentureOrderMapper.selectPage(new Page<FixedDentureOrder>(current, PAGE_SIZE), new QueryWrapper<FixedDentureOrder>()
                .select("id", "patient", "sex", "age", "username", "charge", "order_receiving_date", "delivery_date", "status")
                .eq(bStatus, "status", status)
                .eq(bDoctorId, "username", doctorUsername)
                .orderByDesc("order_receiving_date")
                .orderByDesc("id"));

        for (FixedDentureOrder order : page.getRecords()) {
            //将doctorId 转为 doctorName
            order.setDoctorName(usernameToName(order.getUsername()));
            //将订单状态码转为字符串
            order.setStatusName(OrderStatus.getOrderStatusByState(order.getStatus()).getInfo());
        }

        return page;
    }

    @Override
    public List<FixedDentureOrder> getOrderSearch(String doctorUsername, String patient) {

        List<FixedDentureOrder> list = fixedDentureOrderMapper.selectList(new QueryWrapper<FixedDentureOrder>()
                .select("id", "patient", "sex", "age", "username", "charge", "order_receiving_date", "delivery_date", "status")
                .eq("username", doctorUsername)
                .like("patient", patient.trim())
                .orderByDesc("order_receiving_date")
                .orderByDesc("id"));

        //将doctorId 转为 doctorName
        for (FixedDentureOrder order : list) {
            order.setDoctorName(usernameToName(order.getUsername()));
        }

        return list;
    }

    @Override
    public FixedDentureOrder getParticularOrderById(Integer id) throws RemindException {

        FixedDentureOrder order = fixedDentureOrderMapper.selectById(id);

        //
        String canvasImageId = order.getCanvasImageId();
        CanvasImageFileIO canvasImageFileIO = new CanvasImageFileIO();
        order.setCanvasImage(canvasImageFileIO.getCanvasImage(canvasImageId));
        order.setDoctorName(usernameToName(order.getUsername()));
        return order;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderAllocation(Integer id, String allocation) throws DataBaseException {
        FixedDentureOrder fixedDentureOrder = new FixedDentureOrder();
        fixedDentureOrder.setId(id);
        fixedDentureOrder.setAllocation(allocation);
        if (fixedDentureOrderMapper.updateById(fixedDentureOrder) < 1) {
            throw new DataBaseException();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void problemRecord(Integer id, String problemRecord) throws DataBaseException {
        FixedDentureOrder fixedDentureOrder = new FixedDentureOrder();
        fixedDentureOrder.setId(id);
        fixedDentureOrder.setProblemRecord(problemRecord);
        if (fixedDentureOrderMapper.updateById(fixedDentureOrder) < 1) {
            throw new DataBaseException();
        }
    }

    /**
     * 根据医生的用户名获取医生的姓名
     */
    private String usernameToName(String username) {
        UserInformation userInformation = userInformationMapper.selectOne(new QueryWrapper<UserInformation>().eq("username", username));
        return userInformation.getName();
    }


}
