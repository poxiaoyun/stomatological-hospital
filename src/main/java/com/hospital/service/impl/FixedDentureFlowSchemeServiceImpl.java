package com.hospital.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospital.beans.FixedDentureOrder;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.OrderStatus;
import com.hospital.enumerate.Sign;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.map.FixedDentureMapElement;
import com.hospital.mapper.FixedDentureFlowSchemeMapper;
import com.hospital.beans.FixedDentureFlowScheme;
import com.hospital.mapper.FixedDentureOrderMapper;
import com.hospital.mapper.UserInformationMapper;
import com.hospital.service.FixedDentureFlowSchemeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * (FixedDentureFlowScheme)表服务实现类
 *
 * @author makejava
 * @since 2021-08-21 13:46:57
 */
@Service
public class FixedDentureFlowSchemeServiceImpl extends ServiceImpl<FixedDentureFlowSchemeMapper, FixedDentureFlowScheme> implements FixedDentureFlowSchemeService {

    private final FixedDentureOrderMapper fixedDentureOrderMapper;

    private final FixedDentureFlowSchemeMapper fixedDentureFlowSchemeMapper;

    private final UserInformationMapper userInformationMapper;

    public FixedDentureFlowSchemeServiceImpl(FixedDentureOrderMapper fixedDentureOrderMapper, FixedDentureFlowSchemeMapper fixedDentureFlowSchemeMapper, UserInformationMapper userInformationMapper) {
        this.fixedDentureOrderMapper = fixedDentureOrderMapper;
        this.fixedDentureFlowSchemeMapper = fixedDentureFlowSchemeMapper;
        this.userInformationMapper = userInformationMapper;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void orderReceiving(Integer orderId) throws DataBaseException, RemindException {
        //检验订单是否为为未接单状态
        FixedDentureOrder fixedDentureOrder = fixedDentureOrderMapper.selectOne(new QueryWrapper<FixedDentureOrder>()
                .select("id", "status")
                .eq("id", orderId));
        if (!fixedDentureOrder.getStatus().equals(OrderStatus.UNFINISHED.getState())) {
            throw new RemindException("订单已经接收或已经结束");
        }

        //接单后，将订单的状态改为已接单
        fixedDentureOrder.setStatus(OrderStatus.UNDERWAY.getState());
        if (fixedDentureOrderMapper.updateById(fixedDentureOrder) < 1) {
            throw new DataBaseException();
        }

        //创建流程检验表格
        Map<Integer, String> process = FixedDentureMapElement.getProcess();

        //删除该订单的所有流程
        fixedDentureFlowSchemeMapper.delete(new QueryWrapper<FixedDentureFlowScheme>().eq("order_id", orderId));

        for (Map.Entry<Integer, String> entry : process.entrySet()) {
            FixedDentureFlowScheme fixedDentureFlowScheme = new FixedDentureFlowScheme();
            fixedDentureFlowScheme.setOrderId(orderId);
            fixedDentureFlowScheme.setProcessId(entry.getKey());
            //为工序添加标记,如果当前工序是第一道工序时，将标记改为 等待完成
            if (entry.getKey().equals(1)) {
                fixedDentureFlowScheme.setSign(Sign.UNDERWAY.getState());
            } else {
                fixedDentureFlowScheme.setSign(Sign.FINISH.getState());
            }
            if (fixedDentureFlowSchemeMapper.insert(fixedDentureFlowScheme) < 1) {
                throw new DataBaseException();
            }
        }
        //垃圾回收
        System.gc();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void processAccomplish(FixedDentureFlowScheme fixedDentureFlowScheme) throws DataBaseException, RemindException {
        process(fixedDentureFlowScheme, Sign.UNFINISHED.getState());
    }

    private void process(FixedDentureFlowScheme fixedDentureFlowScheme, Integer sign) throws DataBaseException, RemindException {
        Integer orderId = fixedDentureFlowScheme.getOrderId();
        Integer processId = fixedDentureFlowScheme.getProcessId();

        //检验订单是否已经接收
        FixedDentureOrder fixedDentureOrder = fixedDentureOrderMapper.selectOne(new QueryWrapper<FixedDentureOrder>().select("id", "status").eq("id", orderId));
        if (!fixedDentureOrder.getStatus().equals(OrderStatus.UNDERWAY.getState())) {
            throw new RemindException("订单未接收或已经结束");
        }

        //设置完成时间
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        fixedDentureFlowScheme.setCompletionTime(format.format(new Date()));


        //为下一道工序设置标记 为 等待完成
        //如过当前工序不是最后一道工序时，为下一道工序设置标记
        if (processId != FixedDentureMapElement.getProcess().size()) {
            FixedDentureFlowScheme fixedDentureFlowScheme2 = new FixedDentureFlowScheme();
            fixedDentureFlowScheme2.setSign(Sign.UNDERWAY.getState());
            if (fixedDentureFlowSchemeMapper.update(fixedDentureFlowScheme2, new QueryWrapper<FixedDentureFlowScheme>().eq("order_id", orderId).eq("process_id", processId + 1)) < 1) {
                throw new DataBaseException();
            }
        } else {//如果工序为 最后一道工序时，将订单的状态改为已结束
            //接单后，将订单的状态改为交付完工
            fixedDentureOrder.setStatus(OrderStatus.FINISH.getState());
            if (fixedDentureOrderMapper.updateById(fixedDentureOrder) < 1) {
                throw new DataBaseException();
            }
        }

        //添加标记 当前工序已经完成
        fixedDentureFlowScheme.setSign(sign);
        if (fixedDentureFlowSchemeMapper.updateById(fixedDentureFlowScheme) < 1) {
            throw new DataBaseException();
        }
    }

    @Override
    public void processAccomplishSkip(FixedDentureFlowScheme fixedDentureFlowScheme) throws RemindException, DataBaseException {
        fixedDentureFlowScheme.setCompletionTime("");
        process(fixedDentureFlowScheme, Sign.SKIP.getState());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<FixedDentureFlowScheme> getFlowSchemeByOrderId(Integer orderId) {

        List<FixedDentureFlowScheme> fixedDentureFlowSchemes = fixedDentureFlowSchemeMapper.selectList(new QueryWrapper<FixedDentureFlowScheme>()
                .select("id", "process_id", "username", "quantity", "completion_time", "username_surveyor", "sign")
                .eq("order_id", orderId)
                .orderByAsc("process_id"));

        Map<Integer, String> process = FixedDentureMapElement.getProcess();

        for (FixedDentureFlowScheme scheme : fixedDentureFlowSchemes) {
            //将工序id转为工序名
            Integer processId = scheme.getProcessId();
            if (processId == null) {
                scheme.setProcessName("");
            } else {
                scheme.setProcessName(process.get(processId));
            }
            //将技师id和技师检验人id转为对应的名字
            String username = scheme.getUsername();
            if (username == null) {
                scheme.setTechnicianName("");
            } else {
                scheme.setTechnicianName(userInformationMapper.selectOne(new QueryWrapper<UserInformation>().eq("username", username)).getName());
            }
            String usernameSurveyor = scheme.getUsernameSurveyor();
            if (usernameSurveyor == null) {
                scheme.setTechnicianSurveyorName("");
            } else {
                scheme.setTechnicianSurveyorName(userInformationMapper.selectOne(new QueryWrapper<UserInformation>().eq("username", usernameSurveyor)).getName());
            }
        }
        return fixedDentureFlowSchemes;
    }
}
