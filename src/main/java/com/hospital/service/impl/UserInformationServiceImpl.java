package com.hospital.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.UserRole;
import com.hospital.enumerate.UserState;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import com.hospital.mapper.UserInformationMapper;
import com.hospital.service.UserInformationService;
import com.hospital.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author shkstart
 * @date 2021-08-29 13:54
 */
@Service
public class UserInformationServiceImpl extends ServiceImpl<UserInformationMapper, UserInformation> implements UserInformationService {

    @Autowired
    UserInformationMapper userInformationMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean saveUserInformation(UserInformation userInformation) throws RemindException, DataBaseException {
        String username = userInformation.getUsername();
        int usernameCount = count(new QueryWrapper<UserInformation>().eq("username", username));
        if (usernameCount > 0) {
            throw new RemindException("用户名 " + username + " 已经存在");
        }
        Integer roleId = userInformation.getRoleId();
        if (roleId != 1 && roleId != 2) {
            throw new RemindException("角色字段错误");
        }
        // 用户状态正常
        userInformation.setState(UserState.NORMAL.getState());
        boolean save = save(userInformation);
        if (!save) {
            throw new DataBaseException();
        }
        return true;
    }

    @Override
    public List<UserInformation> getUserList(UserRole role) {
        return list(new QueryWrapper<UserInformation>().select("username", "name", "division").eq("state", UserState.NORMAL.getState()).eq("role_id", role.getState()));
    }

    @Override
    public boolean verifyPassword(String password) {
        Integer id = ShiroUtils.getCurrentUserInformation().getId();
        UserInformation userInformation = userInformationMapper.selectOne(new QueryWrapper<UserInformation>().select("password").eq("id", id));
        return userInformation.getPassword().equals(password);
    }

    @Override
    public boolean removeUserInformation(String username) throws RemindException, DataBaseException {

        Integer count = userInformationMapper.selectCount(new QueryWrapper<UserInformation>().eq("username", username));
        if (count < 1) {
            throw new RemindException("该用户不存在");
        }
        if (userInformationMapper.delete(new QueryWrapper<UserInformation>().eq("username", username)) < 1) {
            throw new DataBaseException();
        }
        return true;
    }
}
