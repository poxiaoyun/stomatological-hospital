package com.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hospital.beans.FixedDentureFlowScheme;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;

import java.util.List;
import java.util.Map;

/**
 * (FixedDentureFlowScheme)表服务接口
 *
 * @author makejava
 * @since 2021-08-21 13:46:57
 */
public interface FixedDentureFlowSchemeService extends IService<FixedDentureFlowScheme> {

    /**
     * 接收订单，并初始化创建义齿流程检验单（紧接着需要分配技师）
     */
    void orderReceiving(Integer orderId) throws DataBaseException, RemindException;


    /**
     * 工序完成，按顺序一道一道完成，如果此工序是最后一道工序时，订单状态改为已结束
     */
    void processAccomplish(FixedDentureFlowScheme fixedDentureFlowScheme) throws DataBaseException, RemindException;

    /**
     * 当前工序跳过
     */
    void processAccomplishSkip(FixedDentureFlowScheme fixedDentureFlowScheme) throws RemindException, DataBaseException;


    /**
     * 查找指定订单的流程检验表
     *
     * @param orderId
     * @return
     */
    List<FixedDentureFlowScheme> getFlowSchemeByOrderId(Integer orderId);

}
