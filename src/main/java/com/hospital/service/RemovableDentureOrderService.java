package com.hospital.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hospital.beans.RemovableDentureOrder;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * (RemovableDentureOrder)表服务接口
 *
 * @author 破晓
 * @since 2021-08-16 21:33:26
 */
public interface RemovableDentureOrderService extends IService<RemovableDentureOrder> {

    /**
     * 添加新订单（需要工厂那边接单）
     */
    void addOrder(RemovableDentureOrder removableDentureOrder, MultipartFile multipartFile) throws DataBaseException, RemindException, IOException;

    /**
     * 删除订单 （订单状态为未接单时才可以删除）（并且只有主任才有权力删除订单）
     */
    void removeOrder(Integer id) throws DataBaseException;

    /**
     * 分页查看指定条数的订单
     *
     * @param current 当前页面
     * @param status 订单的状态
     * @return
     */
    Page<RemovableDentureOrder> getOrderPaging(Integer current, String doctorUsername, Integer status);


    /**
     *  根据医生id和患者姓名查找订单
     *
     * @param doctorUsername 医生id
     * @param patient 患者姓名
     * @return
     */
    List<RemovableDentureOrder> getOrderSearch(String doctorUsername, String patient);


    /**
     * 查询订单的详细信息
     *
     * @param id 订单id
     * @return
     */
    RemovableDentureOrder getParticularOrderById(Integer id) throws RemindException;

    /**
     * 为指定订单写入分配的详细信息
     * @param id
     * @param allocation
     */
    void orderAllocation(Integer id, String allocation) throws DataBaseException;

    /**
     * 问题记录
     */
    void problemRecord(Integer id, String problemRecord) throws DataBaseException;


}
