package com.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hospital.beans.RemovableDentureFlowScheme;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;

import java.util.List;
import java.util.Map;

/**
 * (RemovableDentureFlowScheme)表服务接口
 *
 * @author 破晓
 * @since 2021-08-16 21:32:22
 *
 * 义齿流程检验单
 *
 * 医院发出订单后，工厂可以查看所有的订单，接收订单并创建义齿流程检验单
 *
 */
public interface RemovableDentureFlowSchemeService extends IService<RemovableDentureFlowScheme> {

    /**
     * 接收订单，并初始化创建义齿流程检验单（紧接着需要分配技师）
     */
    void orderReceiving(Integer orderId) throws DataBaseException, RemindException;


    /**
     * 工序完成，按顺序一道一道完成，如果此工序是最后一道工序时，订单状态改为已结束
     *
     * @param removableDentureFlowScheme
     */
    void processAccomplish(RemovableDentureFlowScheme removableDentureFlowScheme) throws DataBaseException, RemindException;


    /**
     * 当前工序跳过
     *
     * @param removableDentureFlowScheme
     */
    void processAccomplishSkip(RemovableDentureFlowScheme removableDentureFlowScheme) throws RemindException, DataBaseException;


    /**
     * 查找指定订单的流程检验表
     *
     * @param orderId
     * @return
     */
    List<RemovableDentureFlowScheme> getFlowSchemeByOrderId(Integer orderId);

}
