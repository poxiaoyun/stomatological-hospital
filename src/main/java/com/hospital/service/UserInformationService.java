package com.hospital.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.UserRole;
import com.hospital.exception.DataBaseException;
import com.hospital.exception.RemindException;

import java.util.List;

/**
 * @author shkstart
 * @date 2021-08-29 13:54
 */
public interface UserInformationService extends IService<UserInformation> {

    /**
     * 添加用户，添加的用户角色有两种（医院、工厂）
     */
    boolean saveUserInformation(UserInformation userInformation) throws RemindException, DataBaseException;

    /**
     * 根据角色获取所有用户信息
     */
    List<UserInformation> getUserList(UserRole role);

    /**
     * 验证密码和当前用户的正确性
     */
    boolean verifyPassword(String password);

    /**
     * 删除用户信息根据 username
     */
    boolean removeUserInformation(String username) throws RemindException, DataBaseException;
}
