package com.hospital.mapper;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.beans.RemovableDentureFlowScheme;

/**
 * (RemovableDentureFlowScheme)表数据库访问层
 *
 * @author 破晓
 * @since 2021-08-16 21:32:22
 */
@Repository
public interface RemovableDentureFlowSchemeMapper extends BaseMapper<RemovableDentureFlowScheme> {
}
