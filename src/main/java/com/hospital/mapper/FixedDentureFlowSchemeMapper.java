package com.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.beans.FixedDentureFlowScheme;
import org.springframework.stereotype.Repository;

/**
 * (FixedDentureFlowScheme)表数据库访问层
 *
 * @author makejava
 * @since 2021-08-21 13:46:57
 */
@Repository
public interface FixedDentureFlowSchemeMapper extends BaseMapper<FixedDentureFlowScheme> {

}
