package com.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.beans.FixedDentureOrder;
import org.springframework.stereotype.Repository;

/**
 * (FixedDentureOrder)表数据库访问层
 *
 * @author makejava
 * @since 2021-08-21 13:46:33
 */
@Repository
public interface FixedDentureOrderMapper extends BaseMapper<FixedDentureOrder> {

}
