package com.hospital.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.beans.UserInformation;
import org.springframework.stereotype.Repository;

/**
 * @author shkstart
 * @date 2021-08-29 13:53
 */
@Repository
public interface UserInformationMapper extends BaseMapper<UserInformation> {
}
