package com.hospital.mapper;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hospital.beans.RemovableDentureOrder;

/**
 * (RemovableDentureOrder)表数据库访问层
 *
 * @author 破晓
 * @since 2021-08-16 21:33:26
 */
@Repository
public interface RemovableDentureOrderMapper extends BaseMapper<RemovableDentureOrder> {
}
