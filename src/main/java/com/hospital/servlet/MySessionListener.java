package com.hospital.servlet;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;

import java.io.Serializable;

/**
 * @author 破晓
 * @date 2021/8/3 - 17:36
 */
@Slf4j
public class MySessionListener implements SessionListener {
    @Override
    public void onStart(Session session) {
        Serializable sessionId = session.getId();
        log.info("=============绘画创建：" + sessionId);
    }

    @Override
    public void onStop(Session session) {
        Serializable sessionId = session.getId();
        log.info("=============会话销毁：" + sessionId);
    }

    @Override
    public void onExpiration(Session session) {
        Serializable sessionId = session.getId();
        log.info("=============会话终结：" + sessionId);
    }
}
