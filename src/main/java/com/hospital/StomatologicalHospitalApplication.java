package com.hospital;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@MapperScan("com.hospital.mapper")
@EnableTransactionManagement
public class StomatologicalHospitalApplication {

    public static void main(String[] args) {
        SpringApplication.run(StomatologicalHospitalApplication.class, args);
    }

}
