package com.hospital.enumerate;

/**
 * @author shkstart
 * @date 2021-08-29 13:38
 */
public enum UserRole {

    NULL(0, "空"),
    HOSPITAL(1, "医院"),
    PLANT(2, "工厂"),
    SUPER_TUBE(3, "超管");

    private Integer state;

    private String info;

    UserRole(Integer state, String info) {
        this.state = state;
        this.info = info;
    }

    public Integer getState() {
        return state;
    }

    public String getInfo() {
        return info;
    }

    public static UserRole getRoleByState(Integer state){
        for (UserRole role : UserRole.values()) {
            if (role.getState().equals(state)){
                return role;
            }
        }
        return UserRole.NULL;
    }
}
