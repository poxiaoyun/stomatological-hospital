package com.hospital.enumerate;

/**
 * @author shkstart
 * @date 2021-08-17 18:00
 *
 * 订单的状态
 * 1：工厂那边未接单
 * 2：工厂那边已经接单
 * 3：义齿已经完成，交付完工
 */
public enum OrderStatus {

    NULL(0, "空"),
    UNFINISHED(1, "未接单"),
    UNDERWAY(2, "已接单"),
    FINISH(3, "已结束");

    private Integer state;

    private String info;

    OrderStatus(Integer state, String info) {
        this.state = state;
        this.info = info;
    }

    public Integer getState() {
        return state;
    }

    public String getInfo() {
        return info;
    }

    public static OrderStatus getOrderStatusByState(Integer state){
        for (OrderStatus orderStatus : OrderStatus.values()) {
            if (orderStatus.getState().equals(state)){
                return orderStatus;
            }
        }
        return OrderStatus.NULL;
    }
}
