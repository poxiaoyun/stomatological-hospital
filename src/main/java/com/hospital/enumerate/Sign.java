package com.hospital.enumerate;

/**
 * @author shkstart
 * @date 2021-08-29 0:42
 */
public enum Sign {

    NULL(0, "空"),
    UNFINISHED(1, "已完成"),
    UNDERWAY(2, "等待完成"),
    FINISH(3, "未完成"),
    SKIP(4, "已跳过");
    private Integer state;

    private String info;

    Sign(Integer state, String info) {
        this.state = state;
        this.info = info;
    }

    public Integer getState() {
        return state;
    }

    public String getInfo() {
        return info;
    }

    public static Sign getSignByState(Integer state){
        for (Sign orderStatus : Sign.values()) {
            if (orderStatus.getState().equals(state)){
                return orderStatus;
            }
        }
        return Sign.NULL;
    }
}
