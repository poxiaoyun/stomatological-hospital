package com.hospital.enumerate;

/**
 * @author shkstart
 * @date 2021-08-29 13:38
 *
 * 保存用户的状态码
 */
public enum UserState {

    NULL(0, "空"),
    NORMAL(1, "正常"),
    FREEZE(2, "冻结");

    private Integer state;

    private String info;

    UserState(Integer state, String info) {
        this.state = state;
        this.info = info;
    }

    public Integer getState() {
        return state;
    }

    public String getInfo() {
        return info;
    }

    public static UserState getRoleByState(Integer state){
        for (UserState role : UserState.values()) {
            if (role.getState().equals(state)){
                return role;
            }
        }
        return UserState.NULL;
    }
}
