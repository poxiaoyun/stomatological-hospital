package com.hospital.shiro;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hospital.beans.UserInformation;
import com.hospital.enumerate.UserRole;
import com.hospital.enumerate.UserState;
import com.hospital.service.UserInformationService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.stereotype.Component;

import java.util.HashSet;

/**
 * @author 破晓
 * @date 2021/7/30 - 0:59
 */
@Component
public class MyRealm extends AuthorizingRealm {

    private final UserInformationService userInformationService;

    public MyRealm(UserInformationService userInformationService) {
        this.userInformationService = userInformationService;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        UserInformation userInformation = (UserInformation) principalCollection.getPrimaryPrincipal();
        Integer roleId = userInformation.getRoleId();
        HashSet<String> roles = new HashSet<>();
        // 医院
        if (UserRole.HOSPITAL.getState().equals(roleId)) {
            roles.add("HOSPITAL");
        }
        // 工厂
        if (UserRole.PLANT.getState().equals(roleId)) {
            roles.add("PLANT");
        }
        // 超管
        if (UserRole.SUPER_TUBE.getState().equals(roleId)) {
            roles.add("HOSPITAL");
            roles.add("PLANT");
            roles.add("ADMIN");
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        return info;
    }

    @Override
    protected SimpleAuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //将AuthenticationToken强转为UsernamePasswordToken
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

        //获取客户端传来的username
        String username = token.getUsername();

        //从数据库查找管理员adminId的信息
        UserInformation userInformation = userInformationService.getOne(new QueryWrapper<UserInformation>().eq("username", username));

        //如果adminInformation为null则用户不存在
        if (userInformation == null || userInformation.getState().equals(UserState.FREEZE.getState())) {
            throw new UnknownAccountException("账号或密码错误！");
        }
        return new SimpleAuthenticationInfo(userInformation, userInformation.getPassword(), getName());
    }
}
