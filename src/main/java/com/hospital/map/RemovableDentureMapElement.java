package com.hospital.map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shkstart
 * @date 2021-08-18 22:46
 */
public class RemovableDentureMapElement {

    //项目
    private static final Map<Integer, String> items = new HashMap<>();
    //材料
    private static final Map<Integer, String> materials = new HashMap<>();
    //工序
    private static final Map<Integer, String> process = new HashMap<>();

    static {
        //加载项目
        items();
        //加载材料
        materials();
        //加载工序
        process();
    }

     private static void items(){
        items.put(1, "铸造支架");
        items.put(2, "胶托义齿");
        items.put(3, "加网");
        items.put(4, "弹性义齿");
        items.put(5, "附着体");
        items.put(6, "套筒冠");
        items.put(7, "颌垫");
        items.put(8, "聚合瓷");
    }

    private static void materials(){
        materials.put(1, "钴铬合金");
        materials.put(2, "钛合金");
        materials.put(3, "拜耳牙");
        materials.put(4, "一般塑料牙");
        materials.put(5, "日本松风牙");
        materials.put(6, "纯钛");
        materials.put(7, "泸鸽牙");
        materials.put(8, "其他");
    }

    private static void process(){
        //铸造支架
        process.put(1, "模型设计");
        process.put(2, "复模");
        process.put(3, "支架蜡型");
        process.put(4, "包埋");
        process.put(5, "铸造");
        process.put(6, "喷砂");
        process.put(7, "试戴");
        process.put(8, "打磨");
        //脱胶|隐形
        process.put(9, "排牙");
        process.put(10, "基托蜡型");
        process.put(11, "装盒");
        process.put(12, "抛光");
    }


    public static Map<Integer, String> getItems() {
        return items;
    }

    public static Map<Integer, String> getMaterials() {
        return materials;
    }

    public static Map<Integer, String> getProcess() {
        return process;
    }
}
