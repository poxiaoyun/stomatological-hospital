package com.hospital.map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shkstart
 * @date 2021-08-21 14:50
 */
public class FixedDentureMapElement {

    //项目
    private static final Map<Integer, String> items = new HashMap<>();
    //材料
    private static final Map<Integer, String> materials = new HashMap<>();
    //烤瓷冠设计要求
    private static final Map<Integer, String> baked_porcelain_crown = new HashMap<>();
    //桥体设计
    private static final Map<Integer, String> the_bridge_design = new HashMap<>();
    //邻间隙
    private static final Map<Integer, String> interproximal_clearance = new HashMap<>();
    //咬合关系
    private static final Map<Integer, String> occluding_relation = new HashMap<>();
    //邻接关系
    private static final Map<Integer, String> syntopy = new HashMap<>();
    //工序
    private static final Map<Integer, String> process = new HashMap<>();

    static {
        //加载项目
        items();
        //加载材料
        materials();
        //烤瓷冠设计要求
        bakedPorcelainCrown();
        //桥体设计
        theBridgeDesign();
        //邻间隙
        interproximalClearance();
        //咬合关系
        occludingRelation();
        //邻接关系
        syntopy();
        //加载工序
        process();
    }

    private static void items() {
        items.put(1, "全瓷冠");
        items.put(2, "金属烤瓷冠");
        items.put(3, "铸造冠");
        items.put(4, "嵌体");
        items.put(5, "桩核");
        items.put(6, "肩合瓷");
        items.put(7, "牙龈瓷");
        items.put(8, "镀金");
        items.put(9, "颌垫");
        items.put(10, "排牙");
        items.put(11, "基台");
    }

    private static void bakedPorcelainCrown() {
        baked_porcelain_crown.put(1, "舌侧金属边缘");
        baked_porcelain_crown.put(2, "全金属边缘");
        baked_porcelain_crown.put(3, "金属咬合(舌)面");
        baked_porcelain_crown.put(4, "全包瓷");
        baked_porcelain_crown.put(5, "单冠");
        baked_porcelain_crown.put(6, "联冠");
    }

    private static void theBridgeDesign() {
        the_bridge_design.put(1, "鞍式");
        the_bridge_design.put(2, "改良盖嵴式");
        the_bridge_design.put(3, "嵌入式");
        the_bridge_design.put(4, "悬空式");
        the_bridge_design.put(5, "船底式");
    }

    private static void interproximalClearance() {
        interproximal_clearance.put(1, "正常(点)");
        interproximal_clearance.put(2, "关闭(面)");
    }

    private static void occludingRelation() {
        occluding_relation.put(1, "紧");
        occluding_relation.put(2, "松");
        occluding_relation.put(3, "无");
    }

    private static void syntopy() {
        syntopy.put(1, "紧");
        syntopy.put(2, "适中");
        syntopy.put(3, "无");
    }

    private static void materials() {
        materials.put(1, "钴铬合金");
        materials.put(2, "镍铬合金");
        materials.put(3, "氧化铝");
        materials.put(4, "铸瓷");
        materials.put(5, "松风聚合瓷");
        materials.put(6, "氧化锆(高)");
        materials.put(7, "氧化锆(中)");
        materials.put(8, "氧化锆(低)");
        materials.put(9, "纯钛");
        materials.put(10, "含钛合金牙");
        materials.put(11, "其他");
    }

    private static void process() {
        //模型预备
        process.put(1, "灌注模型");
        process.put(2, "可卸代型");
        process.put(3, "肩合");
        process.put(4, "上颌架");
        //蜡型
        process.put(5, "基底冠");
        process.put(6, "铸冠嵌体");
        process.put(7, "桩核");
        process.put(8, "包埋");
        //铸造
        process.put(9, "铸造");
        process.put(10, "喷砂");
        //试戴打磨
        process.put(11, "基底冠");
        process.put(12, "全冠嵌体");
        process.put(13, "桩核");
        //烤漆
        process.put(14, "滤色");
        process.put(15, "肩合瓷");
        process.put(16, "堆瓷");
        process.put(17, "修形");
        process.put(18, "上釉");
        process.put(19, "抛光");
        //其他
        process.put(20, "焊接");
        process.put(21, "镀金");
        process.put(22, "甲冠");
        process.put(23, "设计");

    }

    public static Map<Integer, String> getItems() {
        return items;
    }

    public static Map<Integer, String> getMaterials() {
        return materials;
    }

    public static Map<Integer, String> getBaked_porcelain_crown() {
        return baked_porcelain_crown;
    }

    public static Map<Integer, String> getThe_bridge_design() {
        return the_bridge_design;
    }

    public static Map<Integer, String> getInterproximal_clearance() {
        return interproximal_clearance;
    }

    public static Map<Integer, String> getOccluding_relation() {
        return occluding_relation;
    }

    public static Map<Integer, String> getSyntopy() {
        return syntopy;
    }

    public static Map<Integer, String> getProcess() {
        return process;
    }
}
