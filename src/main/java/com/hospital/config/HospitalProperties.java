package com.hospital.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Repository;

/**
 * @author shkstart
 * @date 2021-08-20 19:02
 */
@Repository
@ConfigurationProperties(prefix = "hospital")
public class HospitalProperties {

    //活动义齿订单显示数量
    private Integer removable_denture_order_display_size = 10;

    public Integer getRemovable_denture_order_display_size() {
        return removable_denture_order_display_size;
    }

    public void setRemovable_denture_order_display_size(Integer removable_denture_order_display_size) {
        this.removable_denture_order_display_size = removable_denture_order_display_size;
    }
}
