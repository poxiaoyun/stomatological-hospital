package com.hospital.utils;


import com.hospital.beans.UserInformation;
import org.apache.shiro.SecurityUtils;

/**
 * @author 破晓
 * @date 2021/7/30 - 17:46
 */
public abstract class ShiroUtils {

    public static UserInformation getCurrentUserInformation(){
        return (UserInformation) SecurityUtils.getSubject().getPrincipal();
    }
}
