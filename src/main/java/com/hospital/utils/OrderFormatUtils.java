package com.hospital.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 破晓
 * @date 2021-12-19 16:19
 *
 * 订单编号格式工具类
 */
public abstract class OrderFormatUtils {

    private static final SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyyMMdd");

    /**
     * 按指定格式生成订编号
     * @return 订单编号
     */
    public static int format(String username) {
        String format = simpleDateFormat.format(new Date());
        return Integer.parseInt(format);
    }
}
