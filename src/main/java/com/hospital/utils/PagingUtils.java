package com.hospital.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 破晓
 * @date 2021/7/27 - 20:47
 */
public abstract class PagingUtils {

    private static final int DEFAULT_PAGINATION_COUNT = 5;

    public static List<Long> paginationShow(Page page){

        // 当前页
        long current = page.getCurrent();

        // 当前分页总页数
        long amount = page.getPages();

        List<Long> longs = null;

        if(amount <= 5){
            longs = new ArrayList<>();
            for (int i = 1; i <= amount; i ++){
                longs.add((long) i);
            }
        }else{
            if(current < 3){
                longs = Arrays.asList(1L, 2L, 3L, 4L, 5L);
            }
            if (current > 2 && current < amount-2+1){
                longs = Arrays.asList(current-2, current-1, current, current+1, current+2);
            }
            if(current > amount-2){
                longs = Arrays.asList(amount-4, amount-3, amount-2, amount-1, amount);
            }
        }

        return longs;
    }
}
