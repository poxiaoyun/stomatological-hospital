package com.hospital.beans;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * (RemovableDentureFlowScheme)表实体类
 *
 * @author 破晓
 * @since 2021-08-16 21:32:21
 */
@Data
@TableName("db_removable_denture_flow_scheme")
public class RemovableDentureFlowScheme {
    //自增主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //记录单id（外键）
    private Integer orderId;
    //工序
    private Integer processId;
    //工序名
    @TableField(exist = false)
    private String processName;
    //技工（外键）
    private String username;
    //技工姓名
    @TableField(exist = false)
    private String technicianName;
    //数量
    private Integer quantity;
    //完成时间
    private String completionTime;
    //检验人（外键）
    private String usernameSurveyor;
    //检验人（外键）
    @TableField(exist = false)
    private String technicianSurveyorName;
    //标记（1 已完成 2 等待完成 3 未完成）
    private Integer sign;
}
