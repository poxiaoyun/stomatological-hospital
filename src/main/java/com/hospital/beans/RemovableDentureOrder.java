package com.hospital.beans;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * (RemovableDentureOrder)表实体类
 *
 * @author 破晓
 * @since 2021-08-16 21:33:26
 *
 * 活动义齿订单实体
 */
@Data
@TableName("db_removable_denture_order")
public class RemovableDentureOrder {
    //自增主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //患者
    private String patient;
    //1代表男，2代表女
    private Integer sex;
    //年龄
    private Integer age;
    //医生（外键）
    private String username;
    //医生姓名
    @TableField(exist = false)
    private String doctorName;
    //收费，单位是元
    private Integer charge;
    //接单时间
    private String orderReceivingDate;
    //交付时间
    private String deliveryDate;
    //上颌架
    private String maxillaryFrame;
    //加工费，单位是元
    private Integer cost;
    //备注
    private String comment;
    //加工项目
    private String item;
    //材料
    private String materials;
    //义齿画布base64 id
    private String canvasImageId;
    @TableField(exist = false)
    // 画布base64类型的图片
    private String canvasImage;
    //义齿设计要求
    private String demand;
    //模型文件id
    private String modelId;
    //模型文件路径
    @TableField(exist = false)
    private String modelFilePath;
    //检查是否合格 1代表合格，2代表不合格
    private Integer examine;
    //订单状态（1为未接单，2为已经接单，3为已经完成）
    private Integer status;
    @TableField(exist = false)
    //订单状态
    private String statusName;
    //问题记录（由工厂那边填写）
    private String problemRecord;
    //分配
    private String allocation;
}
