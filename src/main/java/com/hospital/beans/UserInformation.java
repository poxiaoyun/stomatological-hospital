package com.hospital.beans;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.omg.CORBA.INTERNAL;

import java.util.List;

/**
 * @author shkstart
 * @date 2021-08-29 13:51
 */
@Data
@TableName("db_user_information")
public class UserInformation {

    //自增主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //用户名
    private String username;
    //密码
    private String password;
    // 姓名
    private String name;
    // 科室
    private String division;
    //角色id
    private Integer roleId;
    // 用户的状态（正常、冻结）
    private Integer state;
    //指定月的活动义齿数量
    @TableField(exist = false)
    private List<RemovableDentureOrder> RemovableDentureOrderList;
    //指定月的固定义齿数量
    @TableField(exist = false)
    private List<FixedDentureOrder> FixedDentureOrderList;
}
