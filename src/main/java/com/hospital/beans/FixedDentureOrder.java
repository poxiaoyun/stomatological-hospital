package com.hospital.beans;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (FixedDentureOrder)表实体类
 *
 * @author makejava
 * @since 2021-08-21 13:46:32
 *
 * 固定义齿订单实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("db_fixed_denture_order")
public class FixedDentureOrder extends Model<FixedDentureOrder> {
    //自增主键
    @TableId(type = IdType.AUTO)
    private Integer id;
    //患者姓名
    private String patient;
    //1代表男，2代表女
    private Integer sex;
    //年龄
    private Integer age;
    //医生（外键）
    private String username;
    //医生姓名
    @TableField(exist = false)
    private String doctorName;
    //收费，单位是元
    private Integer charge;
    //比色
    private String colorimetric;
    //形态
    private String form;
    //接单时间
    private String orderReceivingDate;
    //交付时间
    private String deliveryDate;
    //上颌架
    private String maxillaryFrame;
    //加工费，单位是元
    private Integer cost;
    //备注
    private String comment;
    //加工项目
    private String item;
    //材料
    private String materials;
    //烤瓷冠设计要求
    private String bakedPorcelainCrown;
    //桥体设计
    private String theBridgeDesign;
    //邻间隙
    private String interproximalClearance;
    //咬合关系
    private String occludingRelation;
    //邻接关系
    private String syntopy;
    //义齿画布base64 id
    private String canvasImageId;
    @TableField(exist = false)
    // 画布base64类型的图片
    private String canvasImage;
    //义齿设计要求
    private String demand;
    //模型文件id
    private String modelId;
    //模型文件路径
    @TableField(exist = false)
    private String modelFilePath;
    //检查是否合格 1代表合格，2代表不合格
    private Integer examine;
    //订单状态（1为未接单，2为已经接单，3为已经完成）
    private Integer status;
    @TableField(exist = false)
    //订单状态
    private String statusName;
    //问题记录（由工厂那边填写）
    private String problemRecord;
    //分配
    private String allocation;

}
