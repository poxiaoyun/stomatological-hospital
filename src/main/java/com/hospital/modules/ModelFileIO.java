package com.hospital.modules;

import com.hospital.exception.RemindException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * @author shkstart
 * @date 2021-08-17 19:45
 */
@Slf4j
public class ModelFileIO extends FileIO {

    //文件夹
    private static final String MODEL_FILE_PATH = "model";

    public ModelFileIO() throws RemindException {
        super(MODEL_FILE_PATH);
    }

    /**
     * 上传文件
     */
    public String uploading(MultipartFile multipartFile) throws RemindException {

        if (multipartFile.isEmpty()) {
            return "";
        }

        String originalFilename = multipartFile.getOriginalFilename();
        log.info("文件原始名:{}", originalFilename);
        log.info("文件名:{}", multipartFile.getName());
        log.info("内容类型:{}", multipartFile.getContentType());

        //获取文件的后缀
        assert originalFilename != null;
        String[] split = originalFilename.split("\\.");
        String fileSuffix = split[split.length - 1];

        UUID fileName = null;
        try {
            fileName = UUID.randomUUID();
            multipartFile.transferTo(new File(file, fileName + "." + fileSuffix));
        } catch (IOException e) {
            throw new RemindException("文件上传失败");
        }
        return fileName.toString() + "." + fileSuffix;
    }

    /**
     * 文件下载
     */
    public void download(String fileName, HttpServletResponse response) throws RemindException, IOException {

        File file = new File(this.file, fileName);
        log.info(file.getPath());
        if (!file.exists()) {
            throw new RemindException("文件不存在");
        }

        FileInputStream is = null;
        BufferedInputStream fis = null;

        try {
            is = new FileInputStream(file);
            fis = new BufferedInputStream(is);
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);

            response.reset();
            response.setCharacterEncoding("utf-8");
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(file.getName(), "UTF-8"));
//            response.addHeader("Content-Disposition", "inline; filename=" + file.getName());
            response.addHeader("Content-Length", "" + file.length());
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            outputStream.write(buffer);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

            if(fis != null){
                fis.close();
            }
            if(is != null){
                is.close();
            }
        }
    }
}
