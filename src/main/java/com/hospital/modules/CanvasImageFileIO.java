package com.hospital.modules;

import com.hospital.exception.RemindException;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

/**
 * @author 破晓
 * @date 2021-12-18 21:43
 */
public class CanvasImageFileIO extends FileIO {

    //文件夹
    private static final String MODEL_FILE_PATH = "canvasImage";

    public CanvasImageFileIO() throws RemindException {
        super(MODEL_FILE_PATH);
    }

    /**
     * 上传文件
     */
    public String uploading(String data) throws RemindException {

        if (data == null) {
            throw new RemindException("数据不可为空");
        }

        UUID fileName = null;
        FileWriter fileWriter = null;
        try {
            fileName = UUID.randomUUID();
            File file = new File(this.file, fileName + ".txt");
            if (!file.exists()) {
                file.createNewFile();
                fileWriter = new FileWriter(file, false);
                fileWriter.write(data);
            }
        } catch (IOException e) {
            throw new RemindException("文件上传失败");
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return fileName.toString() + ".txt";
    }

    /**
     * 获取画布base64字符串
     * @param fileName
     * @return
     */
    public String getCanvasImage(String fileName) {
        StringBuilder result = new StringBuilder();
        String thisLine = null;
        File file = new File(this.file, fileName);
        BufferedReader br = null;
        if (file.exists() && file.isFile()) {
            try {
                br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                while ((thisLine = br.readLine())!= null) {
                    result.append(thisLine);
                }
            }  catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return result.toString();
    }
}
