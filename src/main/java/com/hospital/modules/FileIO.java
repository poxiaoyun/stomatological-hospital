package com.hospital.modules;

import com.hospital.exception.RemindException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @author shkstart
 * @date 2021-08-17 19:46
 */
public abstract class FileIO {


    private final String BASIC_PATH_OF_STATIC_FILES = "static";

    protected String STATIC_FILE_PATH = "";

    protected File file = null;


    public FileIO(String path) throws RemindException {
        this.STATIC_FILE_PATH = System.getProperty("user.dir") + File.separator + BASIC_PATH_OF_STATIC_FILES + File.separator + path;
        this.file = new File(STATIC_FILE_PATH);
        if (!file.exists()){
            if (!file.mkdirs()) {
                throw new RemindException("目录创建失败，请重试");
            }
        }
    }

}
