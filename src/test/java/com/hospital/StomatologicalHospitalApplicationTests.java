package com.hospital;

import com.hospital.config.HospitalProperties;
import com.hospital.exception.DataBaseException;
import com.hospital.service.RemovableDentureOrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class StomatologicalHospitalApplicationTests {

    @Test
    void contextLoads() throws DataBaseException {
    }

}
